const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = async event => {

    let body;
    if(isObject(event.body)){
        body = event.body;
    } else {
        body = JSON.parse(event.body);
    }

    if(body != null){
        switch (event.httpMethod) {
            case 'GET':
                if (body.RequestType === "list") {
                    return getActiveServers();
                } else if (body.RequestType === "details") {
                    return getSingleServerDetails(event, body);
                } else {
                    return {
                        statusCode: 400,
                        body: {
                            message: 'No RequestType found in body'
                        }
                    }
                }
            case 'PUT':
                return createActiveServer(event, body);
            case 'DELETE':
                console.log("calling delete");
                return deleteActiveServer(event, body);
            default:
                return {
                    statusCode: 405,
                    body: {
                        message: 'This server can only accept GET, PUT and DELETE requests'
                    }
                }
        }
    } else {
        return {
            statusCode: 400,
            body: {
                message: 'Cannot find body'
            }
        }
    }
};

const getSingleServerDetails = (event, body) => {

    let Id = "1";
    if(body != null){
        if(body.hasOwnProperty("sessionId")){
            Id = body.sessionId;
        } else {
            return {
                statusCode: 400,
                body:{
                    message:"no sessionId in request"
                }
            }
        }
    } else if (event.hasOwnProperty("sessionId")) {
        Id = event.Id;
    } else {
        return {
            statusCode: 400,
            body:{
                message:"no sessionId in request"
            }
        }
    }

    let params = {
        Key: {
            "sessionId": Id
        },
        TableName: "active_servers"
    };

    return new Promise((resolve, reject) => {
        docClient.get(params, (err, data) => {
            if (!err) {
                console.log(data);
                let response = {
                    "statusCode": 200,
                    "body": JSON.stringify(data)
                };
                resolve(response)
            } else {
                console.log(err, err.stack); // an error occurred
                reject(err)
            }
        });
    })
}

const getActiveServers = () => {

    let params = {
        TableName: "active_servers"
    };

    return new Promise((resolve, reject) => {
        docClient.scan(params, (err, data) => {
            if (!err) {
                console.log(data);
                let response = {
                    "statusCode": 200,
                    "body": JSON.stringify(data)
                };
                resolve(response)
            } else {
                console.log(err, err.stack); // an error occurred
                reject(err)
            }
        });
    })
}

const createActiveServer = (event, body) => {

    if (body.hasOwnProperty("ServerDetails")) {
        let params = {
            "TableName": "active_servers",
            "Item": body.ServerDetails
        };

        return new Promise((resolve, reject) => {
            docClient.put(params, (err, data) => {
                if (!err) {
                    console.log(data);
                    let response = {
                        "statusCode": 200,
                        "body": JSON.stringify(data)
                    };
                    resolve(response)
                } else {
                    console.log(err, err.stack); // an error occurred
                    reject(err)
                }
            });
        })
    }
}

const deleteActiveServer = (event, body) => {

    if (body.hasOwnProperty("sessionId")) {
        let params = {
            "TableName": "active_servers",
            "Key": {
                "sessionId": body.sessionId
            }
        };

        return new Promise((resolve, reject) => {
            docClient.delete(params, (err, data) => {
                if (!err) {
                    console.log(data);
                    let response = {
                        "statusCode": 200,
                        "body": JSON.stringify(data)
                    };
                    resolve(response)
                } else {
                    console.log(err, err.stack); // an error occurred
                    reject(err)
                }
            });
        })
    }
};

function isObject(obj)
{
    return obj !== undefined && obj !== null && obj.constructor === Object;
}